# Maps of Mnemos

A game of memory and adventure!

[Play Maps of Mnemos](https://mom.opbstudios.com)

## Game Rules

The game is played as a series of turns on a grid of an even number of face
down cards, where each turn the player can select two cards to reveal. Depending
on the type of cards revealed different results will happen until the either
the player defeats all the threats or extinguishes their drive for adventure.

The cards are grouped into 4 themes: Armor, Mana, Monsters, and Weapons.

The results of selecting cards is evaluated as follows:

* Both cards are mana, both cards removed from grid, gain 1 mana
* Both cards are armor, both cards removed from grid, lose 1 mana, gain 1 armor
* Both cards are weapon, both cards removed from grid, lose 1 mana, gain 1 Weapon
* Cards do not match, and neither is a monster, both cards returned to face down, lose 1 mana

If a monster is revealed, different things can happen depending on what items
the player has acquired.

* Both cards are monsters,
    * player has weapon, both cards removed from grid, gain 20 mana, lose 1 weapon
    * player has armor, both cards returned to face down, lose 1 mana, lose 1 armor
    * player has nothing, both cards returned to face down, lose 10 mana
* One card is monster, other card is armor, armor is removed from grid and monster
  is returned to face down, lose 1 mana
* One card is monster, other card is weapon, weapon and monster are removed from
  grid, gain 10 mana
* One card is monster, other card is mana,
    * player has weapon, monster removed from grid, mana returned to face down, gain 10 mana, lose 1 weapon
    * player has armor, both cards returned to face down, lose 1 mana, lose 1 armor
    * player has nothing, both cards returned to face down, lose 5 mana

A game ends when all the monsters have been defeated or the player has less than
0 mana.
