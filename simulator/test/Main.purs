module Test.Main where

import Prelude
import Data.List (List(..), (:))
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Aff (launchAff_)
import Game (CP(..), Card(..), CardPos(..), evalMatch, makeGame)
import Test.Spec (Spec, describe, it)
import Test.Spec.Assertions (shouldEqual)
import Test.Spec.Reporter.Console (consoleReporter)
import Test.Spec.Runner (runSpec)

main :: Effect Unit
main = launchAff_ $ runSpec [ consoleReporter ] spec

spec :: Spec Unit
spec =
  describe "Game" do
    it "match mana" $ Tuple Mana Mana `shouldMatch` completedGame { player { mana = 6 } }
    it "defeat monster" $ Tuple Monster Weapon `shouldMatch` completedGame { player { mana = 15 } }
  where
  game = makeGame Nil

  completedGame = game { consumed = p1 : p2 : Nil }

  p1 = CardPos 0

  p2 = CardPos 1

  shouldMatch (Tuple c1 c2) g = evalMatch (Tuple (CP c1 p1) (CP c2 p2)) game `shouldEqual` g
