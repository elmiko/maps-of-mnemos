# simulator

Get purscript and spago by running:

```
npm install -g purescript -g spago
```

Run the [simulation](./src/Main.purs) with node:

```
spago run
```

Run the user interface:

```
npm install parcel
./node_modules/.bin/parcel serve --open src/index.html
```

## References

- Functions documentation: https://pursuit.purescript.org
( see https://discourse.purescript.org/t/tip-configure-web-browser-for-convenient-pursuit-lookup/1561 )
