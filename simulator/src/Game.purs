-- | This module contains the game logic
-- For a given card position and a game, the goal is to produce a new game (or an error) with the `eval` function.
module Game where

import Prelude
import Data.Either (Either(..), note)
import Data.Lens (Lens', lens, (%~))
import Data.List (List(..), elem, (!!), (:))
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..))

data Card
  = Mana
  | Armor
  | Weapon
  | Monster

-- | A new type to define card position
newtype CardPos
  = CardPos Int

data GameState
  = Revealed CardPos
  | Idle

type Game
  = { cards :: List Card
    , state :: GameState
    , consumed :: List CardPos
    , player :: Player
    }

type Player
  = { mana :: Int, armor :: Int, weapon :: Int }

-- | Lenses to get/set player value
-- See https://hackage.haskell.org/package/lens-tutorial-1.0.4/docs/Control-Lens-Tutorial.html
-- The convention for the Lens' type parameters is:
--       +-- Bigger type
--       |
--       v
-- Lens' bigger smaller
--              ^
--              |
--              +--  Smaller type within the bigger type
--
-- We create lenses using the `lens` function by providing a getter and setter.
player :: Lens' Game Player
player = lens (\g -> g.player) (\g p -> g { player = p })

mana :: Lens' Player Int
mana = lens (\p -> p.mana) (\p m -> p { mana = m })

weapon :: Lens' Player Int
weapon = lens (\p -> p.weapon) (\p w -> p { weapon = w })

armor :: Lens' Player Int
armor = lens (\p -> p.armor) (\p a -> p { armor = a })

-- Let's break down this mouthful expression (use `spago repl` to follow at home):
-- * First there is #:
--   > :type (#)
--   a -> (a -> b) -> b
--
-- which lets you write a function application with the parameter on the left side.
-- This is particularly useful for lenses because they provide functions to update an `obj -> obj`
-- so it is convenient to stick the obj on the left side, and chain the lenses on the right side.
-- So instead of writting  `log "hello world"` or `log $ "hello world"` we can write:
--   > import EFfect.Console
--   > "hello world" # log
--
--
-- * Then there is the function composition (which also works for lenses):
--   > :t (<<<)
--   (b -> c) -> (a -> b) -> a -> c
--
-- which lets you compose two functions so that instead of writting `f(g(x))` we can write:
--   (f <<< g) x
-- which is useful to compose lenses. `player <<< mana` becomes a `Lens' Game Int`
--
--
-- * And finally there is `%~` which is the operator version of Data.Lens.over.
--   > :t over
--   Lens' a b -> (b -> b) -> a -> a
--
-- which lets you change an attribute using it's previous value (with the `b -> b` function).
-- So instead of writting `over (player <<< mana) (\prev -> prev + 1)`
-- (which is a function that goes from game to game), we can write:
--   > import Game
--   > :t (player <<< mana %~ (\prev -> prev + 1))
--   Game -> Game
--
-- * Note on operator section (see: https://github.com/purescript/documentation/blob/master/language/Differences-from-Haskell.md#operator-sections)
-- Instead of writing (\x -> x + 1) we can write:
--   > :t (1 + _)
--   Int -> Int
-- This is useful when used with the `%~` operator.
-- | Helper function to update a player value using a lens
updatePlayer :: Lens' Player Int -> Int -> Game -> Game
updatePlayer l value game = game # (player <<< l) %~ (_ + value)

-- | Helper to add 1
gain :: Lens' Player Int -> Game -> Game
gain l = updatePlayer l (1)

-- | Helper to remove 1
lose :: Lens' Player Int -> Game -> Game
lose l = updatePlayer l (-1)

_consumed :: Lens' Game (List CardPos)
_consumed = lens (\g -> g.consumed) (\g c -> g { consumed = c })

-- Note: we don't need to compose two lenses here because _complete works on Game directly.
-- | Helper function to add a consumed card
removeCard :: CardPos -> Game -> Game
removeCard pos game = game # _consumed %~ (pos : _)

-- | Utility function to create a game record
makeGame :: List Card -> Game
makeGame cards = { cards, state: Idle, consumed: Nil, player: { mana: 5, armor: 0, weapon: 0 } }

isGameOver :: Game -> Boolean
isGameOver game = outOfMana || monstersDefeated 0 game.cards
  where
  outOfMana = game.player.mana <= 0

  monstersDefeated pos acc = case acc of
    (Monster : xs)
      | CardPos pos `elem` game.consumed -> monstersDefeated (pos + 1) xs
      | otherwise -> false
    Nil -> true
    (_ : xs) -> monstersDefeated (pos + 1) xs

-- | Utility function to get the Card from a CardPos
getCard :: Game -> CardPos -> Either String Card
getCard game (CardPos pos) = note "Invalid pos" $ game.cards !! pos

-- | The game logic
eval :: CardPos -> Game -> Either String Game
eval userPos game
  | userPos `elem` game.consumed = Left "Already revealed"
  | otherwise = case game.state of
    Idle -> Right $ game { state = Revealed userPos }
    Revealed pos -> do
      card1 <- getCard game pos
      card2 <- getCard game userPos
      pure $ (evalMatch (Tuple (CP card1 pos) (CP card2 userPos)) game) { state = Idle }

-- | A data type to carry both a card and its position
data CP
  = CP Card CardPos

-- | The main game logic
evalMatch :: Tuple CP CP -> Game -> Game
evalMatch cards@(Tuple (CP card1 _) (CP card2 _)) game = updateGame game
  where
  updateGame
    -- the cards are identical
    | card1 == card2 = evalCardsMatch card1
    | otherwise = case monsterMatched cards of
      -- One of the card is a monster
      Just (Tuple monsterPos cp) -> evalMonsterMatch monsterPos cp
      -- Cards do not match, and neither is a monster
      Nothing -> evalNoMatch

  evalCardsMatch card = case card of
    Mana -> bothCardsRemoved cards <<< gain mana
    Armor -> bothCardsRemoved cards <<< lose mana <<< gain armor
    Weapon -> bothCardsRemoved cards <<< lose mana <<< gain weapon
    Monster
      | hasWeapon game -> bothCardsRemoved cards <<< updatePlayer mana (20) <<< lose weapon
      | hasArmor game -> lose mana <<< lose armor
      | otherwise -> updatePlayer mana (-10)

  evalMonsterMatch monsterPos (CP card pos) = case card of
    Armor -> removeCard pos <<< lose mana
    Weapon -> bothCardsRemoved cards <<< updatePlayer mana (10)
    Monster -> evalCardsMatch Monster
    Mana
      | hasWeapon game -> removeCard monsterPos <<< updatePlayer mana (10) <<< lose weapon
      | hasArmor game -> lose mana <<< lose armor
      | otherwise -> updatePlayer mana (-5)

  evalNoMatch = lose mana

bothCardsRemoved :: Tuple CP CP -> Game -> Game
bothCardsRemoved (Tuple (CP _ pos1) (CP _ pos2)) = removeCard pos1 <<< removeCard pos2

hasWeapon :: Game -> Boolean
hasWeapon game = game.player.weapon > 0

hasArmor :: Game -> Boolean
hasArmor game = game.player.armor > 0

-- | Return the monster position and other card when there is a monster
monsterMatched :: Tuple CP CP -> Maybe (Tuple CardPos CP)
monsterMatched cards = case cards of
  Tuple (CP Monster pos) cp -> Just (Tuple pos cp)
  Tuple cp (CP Monster pos) -> Just (Tuple pos cp)
  _ -> Nothing

-- isMonster :: Card -> Card -> Card
-- | Instances to enable equality and comparaison test
derive instance eqCard :: Eq Card

derive instance ordCard :: Ord Card

derive instance eqCardPos :: Eq CardPos

derive instance ordCardPos :: Ord CardPos

derive instance eqState :: Eq GameState

-- | Instances to enable show
instance showCard :: Show Card where
  show card = case card of
    Mana -> "M"
    Armor -> "Ω"
    Weapon -> "↑"
    Monster -> "λ"

instance showPos :: Show CardPos where
  show (CardPos pos) = show pos

instance showGameState :: Show GameState where
  show (Revealed pos) = "Revealed " <> show pos
  show Idle = "Idle"
