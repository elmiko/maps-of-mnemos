-- | This module contains the AI logic to play a game
-- For a given game, the goal is to produce a Walkthrough with the `runAI` function.
module AI where

import Prelude
import Control.Alternative ((<|>))
import Control.Monad.State (State, runState, get, modify_)
import Data.Either (Either(..))
import Data.Lens (Lens', lens, (%~))
import Data.List (List(..), length, notElem, nub, (:))
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..))
import Game (Card(..), CardPos(..), Game, GameState(..), eval, getCard, isGameOver)

type Walkthrough
  = { status :: Status, game :: Game, steps :: Int, moveLogs :: List String }

data Status
  = Completed
  | Pending

-- | Given a 'Game', produces a 'Walkthrough' with a step limit
runAI :: Game -> Int -> Walkthrough
runAI initGame maxStep = walkthrough
  where
  walkthrough =
    let
      --    ( the result,     and state  )
      Tuple { status, game, steps } { moveLogs } = runState (go maxStep initGame) initMemory
    in
      { status, game, steps, moveLogs }

  go 0 game = pure { status: Pending, game: game, steps: maxStep }

  go n game
    | isGameOver game = pure { status: Completed, game, steps: (maxStep - n) }
    | otherwise = do
      newGameM <- play game
      case newGameM of
        Nothing -> pure { status: Pending, game, steps: (maxStep - n) }
        Just newGame -> go (n - 1) newGame

  initMemory :: Memory
  initMemory = { cards: Nil, lastMove: Nothing, moveLogs: Nil }

-- | The AI memory
type Memory
  = { cards :: List (Tuple CardPos Card)
    , lastMove :: Maybe CardPos
    , moveLogs :: List String
    }

_moveLogs :: Lens' Memory (List String)
_moveLogs = lens (\w -> w.moveLogs) (\w ml -> w { moveLogs = ml })

-- | A log to the memory
addMoveLog :: String -> AI Unit
addMoveLog x = modify_ $ (_moveLogs %~ Cons x)

-- | AI is a computation that produces a 'a'
-- using a stateful Memory and an inner context Effect (to print logs on the console)
type AI a
  = State Memory a

-- | Play one step
play :: Game -> AI (Maybe Game)
play game = do
  memory <- get
  let
    move = case game.state of
      Idle
        | length memory.cards == length game.cards -> do
          -- We already clicked all the cards, lets find an unresolved move from the history
          findPreviousMove game memory
        | otherwise -> do
          -- Pick the next unknown card
          nextUnknownCard (length game.cards) memory
      Revealed cur -> do
        -- A card is currently shown, get its value
        card <- getCard game cur
        -- Then find a matching move
        findMatchingMove game memory card
  case move of
    Left err -> do
      addMoveLog $ "No move found: " <> show err
      pure Nothing
    Right (Tuple reason pos) -> do
      addMoveLog $ "Picking card " <> show pos <> ", using: " <> reason
      playMove game pos

type Move
  = Either String (Tuple String CardPos)

-- | Find a move from the history
findPreviousMove :: Game -> Memory -> Move
findPreviousMove game memory = go memory.cards
  where
  go acc = case acc of
    Nil -> nextUnknownCard (length game.cards) memory
    (Tuple pos card : xs)
      | card /= Monster
      , pos `notElem` game.consumed
      , Just pos /= memory.lastMove -> Right $ Tuple "history" pos
      | otherwise -> go xs

-- | A next new move
nextUnknownCard :: Int -> Memory -> Move
nextUnknownCard limit memory = case pos < limit of
  true -> Right $ Tuple "unknown" $ CardPos pos
  false -> Left "Can't find a card to play"
  where
  pos = length memory.cards

-- | Find a move matching an existing card
findMatchingMove :: Game -> Memory -> Card -> Move
findMatchingMove game memory card =
  ( ( case card of
        Monster -> findCard Weapon <|> findCard Armor <|> findCard Mana
        Mana -> findCard Mana
        Armor -> findCard Armor
        Weapon -> findCard Monster <|> findCard Weapon
    )
      <|> getNext
  )
  where
  findCard want = go memory.cards
    where
    go acc = case acc of
      Nil -> Left $ "Can't find" <> show card
      (Tuple pos card' : xs)
        | want == card'
        , game.state /= Revealed pos
        , pos `notElem` game.consumed -> Right (Tuple "memory" pos)
        | otherwise -> go xs

  getNext = findPreviousMove game memory

-- | A function to play the chosen move
playMove :: Game -> CardPos -> AI (Maybe Game)
playMove game pos = do
  let
    action = do
      newGame <- eval pos game
      newCard <- getCard game pos
      pure $ Tuple newGame newCard
  case action of
    Left err -> do
      -- invalid move
      addMoveLog $ "Oops: " <> err
      pure Nothing
    Right (Tuple newGame newCard) -> do
      -- update the memory with the new discovered card
      modify_ (\memory -> memory { lastMove = Just pos, cards = nub <<< Cons (Tuple pos newCard) $ memory.cards })
      -- show what happened
      addMoveLog $ "-> "
        <> case newGame.state of
            Idle -> "matched with " <> show newCard <> ": " <> show newGame.player
            _ -> "picked: " <> show newCard
      -- return the new game
      pure $ Just $ newGame
