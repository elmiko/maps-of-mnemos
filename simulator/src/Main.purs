-- | The main simulation entrypoint
module Main where

import Prelude
import AI (Status(..), Walkthrough, runAI)
import Data.List (List(..), filter, fold, fromFoldable, length, (:))
import Data.List.Lazy (replicate)
import Data.List.Types (NonEmptyList(..))
import Data.NonEmpty ((:|))
import Data.Traversable (traverse, traverse_)
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Console (log)
import Effect.Random (randomInt)
import Game (Card(..), makeGame)
import Test.QuickCheck (mkSeed)
import Test.QuickCheck.Gen (Gen, evalGen, frequency, listOf, suchThat)

-- | The spago run entrypoint
main :: Effect Unit
main = do
  -- Run the simulation
  walkthroughs <- runManySimulation 13 defaultInput
  -- Display the results
  traverse_ (log <<< showWalkthrough) walkthroughs

type SimulationInput
  = { boardSize :: Int
    , monsterFreq :: Number
    }

defaultInput :: SimulationInput
defaultInput = { boardSize: 8 * 8, monsterFreq: 0.5 }

runManySimulation :: Int -> SimulationInput -> Effect (List Walkthrough)
runManySimulation count si = traverse run xs
  where
  xs :: List SimulationInput
  xs = fromFoldable $ replicate count si

run :: SimulationInput -> Effect Walkthrough
run si = do
  -- Generate a board
  seed <- randomInt 0 65535
  board <- pure $ evalGen (generateBoard si) { newSeed: mkSeed seed, size: 100 }
  -- Run the AI
  pure $ runAI (makeGame board) 200

showWalkthrough :: Walkthrough -> String
showWalkthrough walkthrough =
  result
    <> " in "
    <> show walkthrough.steps
    <> " steps for player "
    <> show walkthrough.game.player
    <> " board: "
    <> inf
  where
  inf = showBoardInfo walkthrough.game.cards

  result = case walkthrough.status of
    Completed
      | walkthrough.game.player.mana <= 0 -> "💀 Lost"
      | otherwise -> "✅ Completed"
    Pending -> "❌ Failed to finish the game"

showBoardInfo :: List Card -> String
showBoardInfo cards =
  fold
    $ [ show $ length cards, " cards: " ]
    <> [ count Monster, " monsters, " ]
    <> [ count Weapon, " weapons, " ]
    <> [ count Mana, " mana: " ]
    <> [ show cards ]
  where
  count card = show $ countCard card cards

countCard :: Card -> List Card -> Int
countCard card cards = length $ filter (\c -> c == card) cards

-- | Generate a Card based on a frequency tables
generateCard :: Number -> Gen Card
generateCard monsterFreq = frequency cards
  where
  cards =
    NonEmptyList
      $ Tuple 5.0 (pure Mana)
      :| ( Tuple monsterFreq (pure Monster)
            : Tuple 1.0 (pure Weapon)
            : Tuple 1.0 (pure Armor)
            : Nil
        )

-- | Generate a list of cards
generateBoard :: SimulationInput -> Gen (List Card)
generateBoard si = listOf si.boardSize (generateCard si.monsterFreq) `suchThat` weaponGreaterThanMonster
  where
  weaponGreaterThanMonster cards = countCard Weapon cards >= countCard Monster cards
