-- | This module contains an halogen component to render simulation results
-- Run `parcel serve --open ./index.html`.
module UI (main) where

import Prelude
import Data.Array (concat, fromFoldable, mapWithIndex, reverse)
import Data.Lens (Lens', lens, (%~), (.~))
import Data.List (List(..))
import Effect (Effect)
import Effect.Class (class MonadEffect)
import Halogen (lift, liftEffect)
import Halogen as H
import Halogen.Aff (awaitBody, runHalogenAff)
import Halogen.HTML as HH
import Halogen.HTML.Core (ClassName(..))
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.VDom.Driver (runUI)
import Main (defaultInput, run, showBoardInfo, showWalkthrough)
import AI (Walkthrough)

type State
  = { walkthroughs :: List Walkthrough
    , focus :: Int
    }

_walkthroughs :: Lens' State (List Walkthrough)
_walkthroughs = lens (\s -> s.walkthroughs) (\s w -> s { walkthroughs = w })

_focus :: Lens' State Int
_focus = lens (\s -> s.focus) (\s f -> s { focus = f })

data Action
  = Run
  | Clear
  | Focus Int

component :: forall query input output m. MonadEffect m => H.Component query input output m
component =
  H.mkComponent
    { initialState
    , render
    , eval: H.mkEval $ H.defaultEval { handleAction = handleAction }
    }
  where
  initialState = const { walkthroughs: Nil, focus: -1 }

  render state = HH.div [ HP.class_ (ClassName "container") ] (nav <> body <> results)
    where
    nav =
      [ HH.nav
          [ HP.class_ (ClassName "navbar navbar-expand-lg navbar-light bg-light") ]
          [ HH.a [ HP.class_ (ClassName "navbar-brand") ] [ HH.text "MoM Simulator" ]
          ]
      ]

    body =
      [ HH.button [ HE.onClick \_ -> Run ] [ HH.text "Run" ]
      , HH.button [ HE.onClick \_ -> Clear ] [ HH.text "Clear" ]
      ]

    results = [ HH.ul_ $ concat $ mapWithIndex renderWalkthrough (fromFoldable state.walkthroughs) ]

    renderWalkthrough idx wt =
      [ HH.li_
          [ HH.dl_
              $ [ HH.dt_ [ HH.text "game" ]
                , HH.dd_ [ HH.text $ showBoardInfo $ wt.game.cards ]
                , HH.dt_ [ HH.text "result" ]
                , HH.dd_ [ HH.text $ showWalkthrough wt ]
                ]
              <> case idx == state.focus of
                  true ->
                    [ HH.button [ HE.onClick \_ -> Focus (-1) ] [ HH.text "Hide debug" ]
                    , HH.dt_ [ HH.text "history" ]
                    , HH.dd_ $ renderHistory (fromFoldable wt.moveLogs)
                    ]
                  false -> [ HH.button [ HE.onClick \_ -> Focus idx ] [ HH.text "Debug" ] ]
          ]
      ]

    renderHistory logs = [ HH.ul_ $ map (\l -> HH.li_ [ HH.text l ]) (reverse logs) ]

  handleAction action = case action of
    Run -> do
      -- Generate a board
      walkthrough <- lift $ liftEffect $ run defaultInput
      H.modify_ $ (_walkthroughs %~ Cons walkthrough) <<< (_focus .~ -1)
    Clear -> do
      H.modify_ $ (_walkthroughs .~ Nil)
    Focus idx -> do
      H.modify_ $ (_focus .~ idx)

main :: Effect Unit
main = do
  runHalogenAff do
    body <- awaitBody
    runUI component unit body
