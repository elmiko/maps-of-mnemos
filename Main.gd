extends Control

enum {
	TURN_NEW,
	TURN_FIRST_SELECTED,
	TURN_SECOND_SELECTED,
	TURN_RESOLVING,
	TURN_GAMEOVER,
	TURN_VICTORY,
}

const MAP_WIDTH = 5
const MAP_HEIGHT = 5
const STARTING_ARMOR = 0
const STARTING_WEAPONS = 0
const STARTING_MANA = 10
const STARTING_MONSTERS = 3
const STARTING_TILES = {
	# Tile.TILE_TYPE_MANA_MULTIPLE: 10,
	Tile.TILE_TYPE_MANA: 14,
	Tile.TILE_TYPE_SWORD: 4,
	Tile.TILE_TYPE_ARMOR: 4,
	Tile.TILE_TYPE_MONSTER: STARTING_MONSTERS,
}

var TileScene = preload("res://Tile.tscn")
var _turn_phase = TURN_NEW
var _card1 = null
var _card2 = null
var _mana = 0
var _weapons = 0
var _armor = 0
var _monsters = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	$Playfield.columns = MAP_WIDTH
	$NewGame.show()

func _hide_menus():
	$GameOver.hide()
	$Victory.hide()
	$NewGame.hide()
	
func _new_game():
	_turn_phase = TURN_NEW
	_card1 = null
	_card2 = null
	_reset_playfield()
	_set_playfield(STARTING_TILES)
	_set_weapons(STARTING_WEAPONS)
	_set_armor(STARTING_ARMOR)
	_set_mana(STARTING_MANA)
	_set_monsters(STARTING_MONSTERS)
	_hide_menus()
	
func _reset_playfield():
	for child in $Playfield.get_children():
		child.queue_free()

func _set_playfield(source_tiles):
	var tilelist = []
	for k in source_tiles.keys():
		for _i in range(source_tiles[k]):
			var t = TileScene.instance()
			t.configure(k)
			tilelist.append(t)
	tilelist.shuffle()
	
	for t in tilelist:
		t.connect("tile_clicked", self, "_on_tile_clicked")
		t.connect("fade_completed", self, "_on_tile_fade_completed")
		$Playfield.add_child(t)

func _on_tile_clicked(tile):
	match _turn_phase:
		TURN_NEW:
			_card1 = tile
			tile.expose()
			_turn_phase = TURN_FIRST_SELECTED
		TURN_FIRST_SELECTED:
			_card2 = tile
			tile.expose()
			_turn_phase = TURN_SECOND_SELECTED
		_:
			pass
			
func _on_tile_fade_completed(tile):
	if _turn_phase == TURN_SECOND_SELECTED && tile == _card2:
		_turn_phase = TURN_RESOLVING
		$ResolveTimer.start()


func _on_ResolveTimer_timeout():
	# cards are the same
	if _card1.tile_type == _card2.tile_type:
		var disable = true
		match _card1.tile_type:
			Tile.TILE_TYPE_MANA:
				_set_mana(_mana + 1)
			Tile.TILE_TYPE_MANA_MULTIPLE:
				_set_mana(_mana + 5)
			Tile.TILE_TYPE_SWORD:
				_set_weapons(_weapons + 1)
				_set_mana(_mana - 1)
			Tile.TILE_TYPE_ARMOR:
				_set_armor(_armor + 1)
				_set_mana(_mana - 1)
			Tile.TILE_TYPE_MONSTER:
				if _weapons > 0:
					_set_mana(_mana + 20)
					_set_weapons(_weapons - 1)
					_set_monsters(_monsters - 2)
				elif _armor > 0:
					_set_armor(_armor - 1)
					_set_mana(_mana - 1)
					disable = false
				else:
					_set_mana(_mana - 10)
					disable = false
			_:
				pass
		if disable:
			_card1.disable()
			_card2.disable()
		else:
			_card1.reset()
			_card2.reset()
		_turn_phase = TURN_NEW
	# cards are not the same
	else:
		# no monsters
		if _card1.tile_type != Tile.TILE_TYPE_MONSTER and _card2.tile_type != Tile.TILE_TYPE_MONSTER:
			_set_mana(_mana - 1)
			_card1.reset()
			_card2.reset()
		# monsters!
		else:
			if _card1.is_weapon() or _card2.is_weapon():
				_set_mana(_mana + 10)
				_set_monsters(_monsters - 1)
				_card1.disable()
				_card2.disable()
			elif _card1.is_armor() or _card2.is_armor():
				if _card1.is_armor():
					_card1.disable()
					_card2.reset()
				else:
					_card1.reset()
					_card2.disable()
			elif _weapons > 0:
				_set_weapons(_weapons - 1)
				_set_mana(_mana + 10)
				_set_monsters(_monsters - 1)
				if _card1.is_monster():
					_card1.disable()
					_card2.reset()
				else:
					_card2.disable()
					_card1.reset()
			elif _armor > 0:
				_set_armor(_armor - 1)
				_card1.reset()
				_card2.reset()
			else:
				_set_mana(_mana - 5)
				_card1.reset()
				_card2.reset()

	_turn_phase = TURN_NEW
	_card2 = null
	_card1 = null
	if _mana <= 0:
		$GameOver.show()
		_turn_phase = TURN_GAMEOVER
	if _monsters <= 0:
		$Victory.show()
		_turn_phase = TURN_VICTORY

func _set_armor(quantity):
	_armor = quantity
	$Armor/Quantity.text = str(_armor)

func _set_monsters(quantity):
	_monsters = quantity

func _set_mana(quantity):
	_mana = quantity
	$Mana/Quantity.text = str(_mana)

func _set_weapons(quantity):
	_weapons = quantity
	$Weapons/Quantity.text = str(_weapons)


func _on_New_GameButton_pressed():
	_new_game()
