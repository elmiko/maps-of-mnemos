class_name Tile
extends Control

signal tile_clicked
signal fade_completed

enum {
	TILE_TYPE_NONE,
	TILE_TYPE_MANA,
	TILE_TYPE_MANA_MULTIPLE,
	TILE_TYPE_SWORD,
	TILE_TYPE_MONSTER,
	TILE_TYPE_ARMOR,
}

const FADE_STEP = 0.2


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var front_texture = ""

var tile_type = TILE_TYPE_NONE

var _back_texture = load("res://assets/icons/treasure-map.png")
var _front_texture = null
var _target_texture = null
var _exposed = false

var _fade_dir = 0
var _fade_weight = 0.0
var _color_transparent = Color(0.2, 0.2, 0.2, 0.0)
var _color_opaque = Color(0.2, 0.2, 0.2, 1.0)
var _color_disabled = Color(0.2, 0.2, 0.2, 0.9)

# Called when the node enters the scene tree for the first time.
func _ready():
	if front_texture != "":
		_set_front_texture(front_texture)
	$Back.texture = _back_texture
	$Fader.color = _color_transparent



func _on_Tile_gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT && event.pressed && $FadeTimer.is_stopped():
			emit_signal("tile_clicked", self)

func _on_FadeTimer_timeout():
	if _fade_dir == 0:
		# fade to opaque
		if _fade_weight < 1.0:
			$Fader.color = _color_transparent.linear_interpolate(_color_opaque, _fade_weight)
			_fade_weight += FADE_STEP
		else:
			#$FadeTimer.stop()
			$Fader.color = _color_opaque
			_fade_weight = 0.0
			_fade_dir = 1
			$Back.texture = _target_texture
	else:
		# fade to transparent
		if _fade_weight < 1.0:
			$Fader.color = _color_opaque.linear_interpolate(_color_transparent, _fade_weight)
			_fade_weight += FADE_STEP
		else:
			# animation finished, target texture is showing
			$FadeTimer.stop()
			$Fader.color = _color_transparent
			if _target_texture == _front_texture:
				_exposed = true
			else:
				_exposed = false
			_fade_weight = 0.0
			_fade_dir = 0
			emit_signal("fade_completed", self)

func _set_front_texture(texture):
		_front_texture = load(texture)

func configure(type):
	match type:
		TILE_TYPE_MANA:
			_set_front_texture("res://assets/icons/drop.png")
		TILE_TYPE_MANA_MULTIPLE:
			_set_front_texture("res://assets/icons/droplets.png")
		TILE_TYPE_SWORD:
			_set_front_texture("res://assets/icons/gladius.png")
		TILE_TYPE_MONSTER:
			_set_front_texture("res://assets/icons/evil-minion.png")
		TILE_TYPE_ARMOR:
			_set_front_texture("res://assets/icons/breastplate.png")
		_:
			type = TILE_TYPE_NONE
	tile_type = type
	
func disable():
	$Fader.color = _color_disabled
	$Fader.mouse_filter = MOUSE_FILTER_STOP
	
func expose():
	if _exposed:
		return
	_target_texture = _front_texture
	$FadeTimer.start()

func is_armor():
	match tile_type:
		TILE_TYPE_ARMOR:
			return true
		_:
			return false

func is_monster():
	match tile_type:
		TILE_TYPE_MONSTER:
			return true
		_:
			return false

func is_weapon():
	match tile_type:
		TILE_TYPE_SWORD:
			return true
		_:
			return false

func reset():
	if !_exposed:
		return
	_target_texture = _back_texture
	$FadeTimer.start()
